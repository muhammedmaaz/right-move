// This file is autogenerated via the `commonjs` Grunt task. You can require() this file in a CommonJS environment.
require('../../libs/transition.js')
require('../../libs/alert.js')
require('../../libs/button.js')
require('../../libs/carousel.js')
require('../../libs/collapse.js')
require('../../libs/dropdown.js')
require('../../libs/modal.js')
require('../../libs/tooltip.js')
require('../../libs/popover.js')
require('../../libs/scrollspy.js')
require('../../libs/tab.js')
require('../../libs/affix.js')