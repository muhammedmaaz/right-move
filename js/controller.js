main_app.controller('login-controller', function($scope, $rootScope) {
    $scope.login_page_btn = function() {
        history.go(-1);
        return false;
    }

    $rootScope.loggedUser = false;

    $scope.myFunc = function() {
        // console.log("sdfsd",$scope.password)

        if ($scope.name == "root" && $scope.password == "root") {

            $rootScope.loggedUser = true;
        } else {
            $scope.loginError = "Invalid User Name or Password"
        }
    }
});



main_app.controller('anaysys', function($scope, $rootScope) {



    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Courses', 'Priority', 'Aspiration'],
            ['Visual Arts', 'Jan', 'Mar'],
            ['Guitar', 'Mar', 'Aug'],
            ['BBA', 'Jun', 'Sept'],
            ['HTML', 'Feb', 'Dec']
        ]);

        var options = {

            legend: { position: "none" },
            chartArea: {
                backgroundColor: '#000'
            },
            chart: {
                title: '',
                subtitle: '',
            },
            bars: 'horizontal', // Required for Material Bar Charts.
            height: 400,
            colors: ['#EA9123', '#227112', '#AEB200']
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions(options));

        // chart.draw(data, options);
    }



});


main_app.controller('ProfileController', function($scope, $rootScope) {


    $(function() {
        $(".draggable2").draggable({ snap: ".ui-widget-header" });
        $(".draggable3").droppable({
            accept: ".draggable2",
            classes: {
                "ui-droppable-active": "ui-state-active",
                "ui-droppable-hover": "ui-state-hover"
            },
            drop: function(event, ui) {
                $(this)
                    .addClass("ui-state-highlight")
                    .find("p")
                    .html("<a href='#/analytics'>CREATE PLAN</a>");
            }
        });




    });

    $scope.logout = function() {
        $rootScope.loggedUser = false;
    }
    $scope.initdate = function() {
        $(function() {
            // wait till load event fires so all resources are available
            $("#datepicker").datepicker();
            $('#basicExample').timepicker();
        });
    };

    $scope.initdate();

    $scope.profileData = {
        profilePic: "images/friends/5.jpg",
        profileName: "Alice Stark",
        Status: "lorem Ipsum",
        profileAddress: "No 99, 4th street,13th Block, Westin",
        ProfilePhoneNo: "94777 47773",
        ProfileEmail: "abdcef@gmail.com",
        ProfileAbout: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.Lorem ipsum dolor sit amet.",
        AcadamicInterest: "Subject",
        HobbiesofInterest: "lorem Ipsum",
        Communityofintetest: "lorem Ipsum",
        GroupofInterest: "lorem Ipsum",
        RoleModels: "lorem Ipsum",
        MyEarning: "lorem Ipsum",
        Photos: "110",
        Videos: "4",
        Documents: "5"
    };

    $scope.TrendsyouMissedtoCatchUp = [
        { trendVal: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }, { trendVal: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }, { trendVal: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }, { trendVal: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }
    ];

    $scope.employmentOpportunities = [
        { opportunity: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }, { opportunity: "Lorem Ipsum is simply dummy text of the printing and typesetting industry." }
    ];


    $scope.friends = [
        { Fimage: "images/friends/1.jpg" }, { Fimage: "images/friends/2.jpg" }, { Fimage: "images/friends/3.jpg" }, { Fimage: "images/friends/4.jpg" }
    ];

    $scope.group = [
        { Gimage: "images/institute/3.jpg", Gtitle: "Law Group", LastUpdated: "22/11/23", Notifications: "22" }, { Gimage: "images/institute/2.jpg", Gtitle: "Law Group", LastUpdated: "22/11/23", Notifications: "22" }
    ];
});
main_app.controller('homePage', function($scope, $rootScope) {

        $scope.initdate = function() {
        $(function() {
            // wait till load event fires so all resources are available
            $("#datepicker").datepicker();
            $('#basicExample').timepicker();
        });
    };

    $scope.initdate();

    $scope.profileData = {
        profilePic: "images/friends/5.jpg",
        profileName: "Alice Stark",
        Status: "lorem Ipsum",
        profileAddress: "No 99, 4th street,13th Block, Westin",
        ProfilePhoneNo: "94777 47773",
        ProfileEmail: "abdcef@gmail.com",
        ProfileAbout: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.Lorem ipsum dolor sit amet.",
        AcadamicInterest: "Subject",
        HobbiesofInterest: "lorem Ipsum",
        Communityofintetest: "lorem Ipsum",
        GroupofInterest: "lorem Ipsum",
        RoleModels: "lorem Ipsum",
        MyEarning: "lorem Ipsum",
        Photos: "110",
        Videos: "4",
        Documents: "5"
    };
    $scope.logout = function() {
        $rootScope.loggedUser = false;
    }
    $scope.inithome = function() {
        $(function() {

            $(".set-fav").on("click", function() {
                if ($(this).hasClass("f-active")) {
                    // console.log("sdfdfdsf");
                    $(this).removeClass("f-active");
                } else {
                    // console.log("illla");
                    $(this).addClass("f-active");
                }


            });

            setTimeout(function() {
                $('#pinBoot').pinterest_grid({
                    no_columns: 4,
                    padding_x: 10,
                    padding_y: 10,
                    margin_bottom: 50,
                    single_column_breakpoint: 700
                });

                $(".star_btn").on("click", function() {
                    if ($(this).parents(".white-panel").hasClass("bookmarked-panel")) {
                        // console.log("sdfdfdsf");
                        $(this).parents(".white-panel").removeClass("bookmarked-panel");
                    } else {
                        // console.log("illla");
                        $(this).parents(".white-panel").addClass("bookmarked-panel");
                    }
                });

            }, 0);
            // wait till load event fires so all resources are available
        });
    };
    $scope.inithome();
    // $scope.starClicked = function(clickId) {
    //     if ($scope.favGride != "bookmarked-panel") { $scope.favGride = "bookmarked-panel"; } else { $scope.favGride = "unBookmarked"; }
    // }

    $scope.homegride = [{
        image: "images/Card-1.png",
        title: "What Suits me Best?",
        description: "Animation, Politics, Hacking, Ice Hockey.",
        gridLink: "sample.com"
    }, {
        image: "images/Card-2.png",
        title: "Tedx Talk",
        description: "How to stay focussed ~ Stancy Lumix",
        gridLink: "tedtalk.com"
    }, {
        image: "images/Card-3.png",
        title: "Learn Guitar in 90 days",
        description: "Online classes for acoustic and Electric Guitar",
        gridLink: "youscian.com"
    }, {
        image: "images/Card-4.png",
        title: "20 Fashion Tips",
        description: "Daily Fashion Tips from lakme institute of Fashion Technology for a trendy girl.",
        gridLink: "sample.com"
    }, {
        image: "images/Card-5.png",
        title: "Football Game",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.",
        gridLink: "sample.com"
    }, {
        image: "images/Card-6.png",
        title: "University of Cambridge",
        description: "Course on Petrology begins from 1st NOV.",
        gridLink: "Cambridgeuniversity.com"
    }, {
        image: "images/Card-7.png",
        title: "Career Steps to be CEO",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.",
        gridLink: "sample.com"
    }, {
        image: "images/Card-8.png",
        title: "Do it yourself",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.",
        gridLink: "sample.com"
    }];
});

main_app.controller('searchPage', function($scope, $rootScope) {
    $scope.profileData = {
        profilePic: "images/friends/5.jpg",
        profileName: "Alice Stark",
        Status: "lorem Ipsum",
        profileAddress: "No 99, 4th street,13th Block, Westin",
        ProfilePhoneNo: "94777 47773",
        ProfileEmail: "abdcef@gmail.com",
        ProfileAbout: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.Lorem ipsum dolor sit amet.",
        AcadamicInterest: "Subject",
        HobbiesofInterest: "lorem Ipsum",
        Communityofintetest: "lorem Ipsum",
        GroupofInterest: "lorem Ipsum",
        RoleModels: "lorem Ipsum",
        MyEarning: "lorem Ipsum",
        Photos: "110",
        Videos: "4",
        Documents: "5"
    };

    $scope.logout = function() {
        $rootScope.loggedUser = false;
    }



    $scope.searchResult = [{
        searchHeading: "College of fine art, Bangalore",
        searchDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.",
        Location: "Bangalore",
        Rating: 4.5,
        Reviews: 46,
        searchResultImg: "images/Image-Card1.png",
        searchResultAddress: "Kumara Karupa Road, Bangaluru - 03",
        CoursesOffered: "Visual Art, Painting, Sculpting, Graphic Design",
        Followers: "20K",
        VisitPageLink: "detail.html",
        type: "Mentors"

    }, {
        searchHeading: "ARTS COLLEGE HUBLI",
        searchDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.",
        Location: "Bangalore",
        Rating: 4.5,
        Reviews: 46,
        searchResultImg: "images/Image-Card2.png",
        searchResultAddress: "Kumara Karupa Road, Bangaluru - 03",
        CoursesOffered: "Visual Art, Painting, Sculpting, Graphic Design",
        Followers: "20K",
        VisitPageLink: "detail.html",
        type: "Mentors"

    }, {
        searchHeading: "VENKATAPPA ART COLLEGE",
        searchDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.",
        Location: "Bangalore",
        Rating: 4.5,
        Reviews: 46,
        searchResultImg: "images/Image-Card3.png",
        searchResultAddress: "Kumara Karupa Road, Bangaluru - 03",
        CoursesOffered: "Visual Art, Painting, Sculpting, Graphic Design",
        Followers: "20K",
        VisitPageLink: "detail.html",
        type: "Institute"
    }, {
        searchHeading: "COLLEGE OF FINE ARTS MYSORE",
        searchDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.",
        Location: "Bangalore",
        Rating: 4.5,
        Reviews: 46,
        searchResultImg: "images/Image-Card4.png",
        searchResultAddress: "Kumara Karupa Road, Bangaluru - 03",
        CoursesOffered: "Visual Art, Painting, Sculpting, Graphic Design",
        Followers: "20K",
        VisitPageLink: "detail.html",
        type: "Institute"
    }, {
        searchHeading: "GOVERNMENT ARTS COLLEGE HYDERABAD",
        searchDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.",
        Location: "Bangalore",
        Rating: 4.5,
        Reviews: 46,
        searchResultImg: "images/Image-Card6.png",
        searchResultAddress: "Kumara Karupa Road, Bangaluru - 03",
        CoursesOffered: "Visual Art, Painting, Sculpting, Graphic Design",
        Followers: "20K",
        VisitPageLink: "detail.html",
        type: "Institute"
    }];
});
