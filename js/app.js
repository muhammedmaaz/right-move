"use strict";
var main_app = angular.module('app', ['ngRoute']);

main_app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'home.html'
        }).
        when('/search-page', {
            templateUrl: 'search-page.html'
        }).
        when('/advanced-search', {
            templateUrl: 'advanced-search.html'
        }).
        when('/advanced-search-result', {
            templateUrl: 'advanced-search-result.html'
        }).
        when('/learning-plan', {
            templateUrl: 'learning-plan.html'
        }).
        when('/mentors-detail', {
            templateUrl: 'mentors-detail.html'
        }).
        when('/mentors', {
            templateUrl: 'mentors.html'
        }).
        when('/profile-page', {
            templateUrl: 'profile-page.html'
        }).
        when('/appoinment', {
            templateUrl: 'appoinment.html'
        }).
        when('/create-a-new-me', {
            templateUrl: 'create-a-new-me.html'
        }).
        when('/notifications', {
            templateUrl: 'notifications.html'
        }).
        when('/mentor-appointment', {
            templateUrl: 'mentor-appointment.html'
        }).
        when('/social', {
            templateUrl: 'social.html'
        }).
        when('/home-gride-detail1', {
            templateUrl: 'home-gride-detail1.html'
        }).
        when('/home-gride-detail2', {
            templateUrl: 'home-gride-detail2.html'
        }).
        when('/home-gride-detail3', {
            templateUrl: 'home-gride-detail3.html'
        }).
        when('/home-gride-detail4', {
            templateUrl: 'home-gride-detail4.html'
        }).
        when('/home-gride-detail5', {
            templateUrl: 'home-gride-detail5.html'
        }).
        when('/home-gride-detail6', {
            templateUrl: 'home-gride-detail6.html'
        }).
        when('/home-gride-detail7', {
            templateUrl: 'home-gride-detail7.html'
        }).
        when('/home-gride-detail8', {
            templateUrl: 'home-gride-detail8.html'
        }).
        when('/login', {
            templateUrl: 'login.html'
        }).
        when('/logged-users', {
            templateUrl: 'logged-users.html'
        }).
        when('/friend-profile', {
            templateUrl: 'friend-profile.html'
        }).
        when('/analytics', {
            templateUrl: 'analytics.html'
        }).
        when('/institute-detail', {
            templateUrl: 'institute-detail.html'
        })

        .
        when('/institute-register', {
            templateUrl: 'institute-register.html'
        }).
        when('/mentor-register', {
            templateUrl: 'mentor-register.html'
        }).
        when('/learner-register', {
            templateUrl: 'learner-register.html'
        })





        // .
        // otherwise({
        //     redirectTo: '/'
        // })
        ;
    }
]);

main_app.run(function($rootScope, $location) {
    // register listener to watch route changes
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        if ($rootScope.loggedUser == false) {

            // no logged user, we should be going to #login
            if (next.templateUrl == "advanced-search.html") {
                // already going to #login, no redirect needed
                $location.path("/login");
            } else {
                // not going to #login, we should redirect now
            }
            if (next.templateUrl == "advanced-search-result.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "learning-plan.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "mentors-detail.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "profile-page.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "appoinment.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "notifications.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "social.html") {
                $location.path("/login");
            } else {}
            // if (next.templateUrl == "analytics.html") {
            //     $location.path("/login");
            // } else {}
            // if (next.templateUrl == "analytics.html") {
            //     $location.path("/login");
            // } else {}
            

            
            if (next.templateUrl == "mentors.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "friend-profile.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "logged-users.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "friend-profile.html") {
                $location.path("/login");
            } else {}
            if (next.templateUrl == "institute-detail.html") {
                $location.path("/login");
            } else {}


            
        } else {}
    });
});
